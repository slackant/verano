this a development application web with python and django,
for registration to summer courses at the chilpancingo technological institute

requirements:

Pillow==2.5.1
django-simple-captcha==0.4.2
jdcal==1.0
oauthlib==0.6.3
openpyxl==1.6.1
python-openid==2.2.5
python-social-auth==0.1.26
requests==2.3.0
requests-oauthlib==0.4.1
six==1.7.3
xlrd==0.9.0
Django==1.8.5
pyjade==3.1.0

