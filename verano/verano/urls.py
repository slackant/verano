from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('home.urls')),
    url(r'^captcha/', include('captcha.urls')),

    # url('', include('django.contrib.auth.urls')),
    # Accounts
    url(r'^accounts/', include('accounts.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^school/', include('horario.urls')),
    # url(r'^accounts/complete/google-oauth2/$', TemplateView.as_view(template_name="accounts/profile.jade")),

    url(r'^admin/', include(admin.site.urls)),
)

