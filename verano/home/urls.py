from django.conf.urls import url, patterns
from django.views.generic import TemplateView

from .views import InstructionsView
from news.views import LatestNews

urlpatterns = patterns(
    '',
    url(r'^$', LatestNews.as_view(), name='index'),
    url(r'^cover$', TemplateView.as_view(template_name='cover.jade'), name='cover'),
    # url(r'^hi$', MaestroImport.as_view(), name='maestro-import'),
    url(r'^instructions/$', InstructionsView.as_view(), name='instructions'),
)

