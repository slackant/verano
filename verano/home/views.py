from django.shortcuts import render

from django.views.generic import TemplateView


class HomeView(TemplateView):
    template_name = 'index.jade'


class InstructionsView(TemplateView):
    template_name = 'instructions.jade'


class AjaxTemplateMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'ajax_template_name'):
            split = self.template_name.split('.jade')
            split[-1] = '_inner'
            split.append('.jade')
            self.ajax_template_name = ''.join(split)
        if request.is_ajax():
            self.template_name = self.ajax_template_name
        return super(AjaxTemplateMixin, self).dispatch(request, *args, **kwargs)

from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator


class StaffRequiredMixin(object):
    """Add user_passes_test functionality to class based views"""
    @method_decorator(user_passes_test(lambda u: u.is_staff))
    def dispatch(self, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(*args, **kwargs)
