from django.conf.urls import url, patterns, include

from django.contrib.auth.views import password_change
from django.views.generic import TemplateView

from .views import LoginView, logout_view, CreateUserView, validate_user
from .forms import ChangePasswordForm

urlpatterns = patterns (
    'accounts.views',
    url(r'', include('social.apps.django_app.urls', namespace='social')),

    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^user/add/$', CreateUserView.as_view(), name='create-user'),
    url(r'^user/validate/(?P<token>[a-z0-9-]+)$', validate_user, name='email-validation'),

    url(r'^password_change_done/$', TemplateView.as_view(
        template_name="accounts/yep.jade"), name='password_change_done'),

    url(r'^password_change/$', password_change, {
            'template_name': 'accounts/change_password.jade',
            'password_change_form': ChangePasswordForm
        }, name='change-passwd'),

    # url(r'^password_change/$', ChangePasswordView.as_view(), name='change-passwd'),
    url(r'^logout/logout_success/$', TemplateView.as_view(
        template_name="accounts/logout.jade"), name="logout-success"),

    url(r'^logout/$', logout_view, name="logout"),
)
