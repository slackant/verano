# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.core.mail import EmailMessage
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import FormView, CreateView

from .forms import AuthForm, CreateUserForm

from home.views import AjaxTemplateMixin
from horario.models import Alumno


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('logout_success')


def validate_user(request, token=None):
    try:
        alumno = Alumno.objects.get(token=token, token_used=False)
        alumno.token_used = True
        alumno.save()
    except:
        return HttpResponse(status=404)

    if alumno:
        try:
            user = User.objects.get(username=alumno.numero_control)
            user.is_active = True
            user.save()

            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return HttpResponseRedirect(reverse_lazy('index'))
        except:
            return HttpResponse(status=404)


class NextUrlMixin(object):
    """Allows to redirect a view to its correct success url."""
    def get_success_url(self):
        if 'next' in self.request.GET:
            return self.request.GET.get('next')
        return reverse_lazy('index') 


class CreateUserView(AjaxTemplateMixin, NextUrlMixin, SuccessMessageMixin,
        CreateView):
    form_class = CreateUserForm
    template_name = 'accounts/create_user.jade'
    success_url = reverse_lazy('index')
    success_message = 'Correo de activacion enviado!'

    def form_valid(self, form):
        response = super(CreateUserView, self).form_valid(form)  # create user

        user = User.objects.get(email=form.cleaned_data['email'])
        user.is_active = False  # set is_active to false, force mail validation
        user.save()

        try:
            alumno = Alumno.objects.get(numero_control=user.username)
            subject = 'Activa tu cuenta'
            content = 'Sigue el sig enlace http://localhost:8000/accounts/user/validate/%s' % alumno.token
            email = EmailMessage(subject, content, to=[user.email])
            email.send()
            # Associate alumno to user.
            alumno.user = user
            alumno.save()
        except:
            pass

        return response


class LoginView(SuccessMessageMixin, NextUrlMixin, FormView):
    template_name = 'accounts/login.jade'
    form_class = AuthForm
    success_message = 'Bienvenido %(username)s'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginView, self).form_valid(form)
