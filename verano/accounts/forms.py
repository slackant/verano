# -*- coding: utf-8 -*-

from django import forms

from django.contrib.auth.forms import (AuthenticationForm, UserCreationForm,
                                       PasswordChangeForm)

from django.contrib.auth.models import User

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

from captcha.fields import CaptchaField

from horario.models import Alumno


class ChangePasswordForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

        self.fields['old_password'].widget.attrs['class'] = 'form-control'
        self.fields['new_password1'].widget.attrs['class'] = 'form-control'
        self.fields['new_password2'].widget.attrs['class'] = 'form-control'


class CreateUserForm(UserCreationForm):
    USERNAME_VALIDATION_MSG = 'Ingrese un numero de control valido'
    username = forms.CharField(
        label=u'Número de control',
        validators=[RegexValidator(regex='[\d]{8}',
                                   message=USERNAME_VALIDATION_MSG,
                                   code='nomatch')],
        max_length=8,
        min_length=8
    )
    email = forms.CharField(label=u"Correo electrónico",
                            max_length=75, required=True)

    captcha = CaptchaField(label=u"¡Captcha!",
                           help_text=u"Introduzca el patrón mostrado arriba")

    def __init__(self, *args, **kwargs):
        super(CreateUserForm, self).__init__(*args, **kwargs)
        self.fields['username'].help_text = u"Ocho caracteres numéricos."
        self.fields['email'].help_text = u"Una cuenta de correo electrónico" \
                                         u" activa donde se le enviarán" \
                                         u" instrucciones para terminar de" \
                                         u" activar esta cuenta."
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['captcha'].widget.attrs['class'] = 'form-control col-md-4'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'

    class Meta(UserCreationForm.Meta):
        fields = ('email', 'username')

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.layout = Layout(
    #         Fieldset('Activar cuenta de usuario',
    #             'username', 'password1', 'password2', 'captcha', 'email'),

    #         # Submit('save', 'Guardar', css_class="btn white",
    #         #        data_target='#social_modal', data_toggle='modal')

    #         Submit('save', 'Guardar', css_class="btn white")
    #     )
    #     return helper

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            user = User.objects.get(email=email)
            raise ValidationError(u'Dirección de correo electrónico ya ha sido'
                                  ' vinculada con otro usuario!')
        except User.DoesNotExist:
            return email

    def clean(self):
        no_control = self.cleaned_data.get('username', None)
        try:
            alumno = Alumno.objects.get(numero_control=no_control)
        except Alumno.DoesNotExist:
            raise forms.ValidationError('Alumno no registrado')

        return super(CreateUserForm, self).clean()


class AuthForm(AuthenticationForm):
    """
    A custom authentication form.
    """
    username = forms.CharField(label=u'Número de control')

    def __init__(self, *args, **kwargs):
        super(AuthForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password'].widget.attrs['class'] = 'form-control'
