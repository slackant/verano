from django.template import Library

register = Library()

@register.filter
def cut_text(text):
    """ Allows to filter the news' text."""
    if len(text) > 130:
        return text[:130] + " ..."
    else:
        return text

