# *-* coding:utf-8 *-*

from django.db import models

# from accounts.models import UserRegister as User
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class Entry(models.Model):
    title = models.CharField(u'título', max_length=200)
    body = models.TextField(_(u'información'))

    published = models.BooleanField('publicar', default=False,
            help_text=_(u"Seleccione esta opción cuando desee que la noticia sea publicada inmediatamente en la aplicación Web"))
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, help_text="Indica el usuario responsable de publicar la presente nota")

    class Meta:
        verbose_name = u"Publicación"
        verbose_name_plural = "Publicaciones"
        ordering = ["-created"]

