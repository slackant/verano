# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, verbose_name='título')),
                ('body', models.TextField(verbose_name='información')),
                ('published', models.BooleanField(help_text='Seleccione esta opción cuando desee que la noticia sea publicada inmediatamente en la aplicación Web', default=False, verbose_name='publicar')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('user', models.ForeignKey(help_text='Indica el usuario responsable de publicar la presente nota', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name_plural': 'Publicaciones',
                'verbose_name': 'Publicación',
            },
        ),
    ]
