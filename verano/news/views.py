from django.shortcuts import render
from django.views.generic import (ListView, DetailView, CreateView,
        UpdateView, DeleteView)
from django.views.generic.dates import ArchiveIndexView, MonthArchiveView
from django.core.urlresolvers import reverse_lazy

from django.contrib.messages.views import SuccessMessageMixin

from home.views import StaffRequiredMixin       

from .forms import *
from .models import Entry


# class LatestNews(ArchiveIndexView):
class LatestNews(ListView):
    queryset = Entry.objects.filter(published=True).order_by('-modified')[:5]
    # model = Entry
    template_name = "index.jade"
    # date_field = 'modified'


class NewsMonthView(ListView):
    model = Entry
    date_field = 'modified'
    queryset = Entry.objects.filter(published=True).order_by('-modified')
    make_object_list = True # 
    template_name = "news/archive.jade"
    paginate_by = 3


class EntryDetailView(DetailView):
    model = Entry
    template_name = "news/detail.jade"


class CreateNew(SuccessMessageMixin, StaffRequiredMixin, CreateView):
    model = Entry
    form_class = EntryForm

    success_url = reverse_lazy('index')
    success_message = "%(title)s was created successfully!"
    template_name = "news/create.jade"
    breadcrumbs = "Crear nueva publicacion"


class EditNewView(StaffRequiredMixin, UpdateView):
    model = Entry
    form_class = EntryForm

    success_url = reverse_lazy("index")
    template_name = 'news/create.jade'
    breadcrumbs = "Editar publicacion"


class DeleteNewView(StaffRequiredMixin, DeleteView):
    model = Entry
    template_name = 'news/delete.jade'
    success_url = reverse_lazy("index")
    breadcrumbs = 'Eliminar publicacion'
