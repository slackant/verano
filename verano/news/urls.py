from django.conf.urls import patterns, url, include
from django.contrib.auth.decorators import login_required

from .views import *

urlpatterns = patterns('',
    url(r'^entry/archive/$', login_required(NewsMonthView.as_view()), name='entry-archive'),
    url(r'^entry/add/', CreateNew.as_view(), name="entry-add"),
    url(r'^entry/(?P<pk>\d+)/$', EntryDetailView.as_view(), name="entry-detail"),
    url(r'^entry/edit/(?P<pk>\d+)/$', EditNewView.as_view(), name="entry-edit"),
    url(r'^entry/delete/(?P<pk>\d+)/$', DeleteNewView.as_view(), name="entry-remove"),
)
