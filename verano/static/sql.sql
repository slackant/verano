BEGIN;
CREATE TABLE "horario_departamento" (
    "id" integer NOT NULL PRIMARY KEY,
    "nombre" varchar(50) NOT NULL UNIQUE,
    "jefe" varchar(100) NOT NULL
)
;
CREATE TABLE "horario_carrera" (
    "id" integer NOT NULL PRIMARY KEY,
    "nombre" varchar(65) NOT NULL UNIQUE,
    "departamento_id" integer NOT NULL REFERENCES "horario_departamento" ("id"),
    "coordinador" varchar(100) NOT NULL,
    "presidente_academico" varchar(100) NOT NULL
)
;
CREATE TABLE "horario_planestudio" (
    "clave" varchar(15) NOT NULL PRIMARY KEY,
    "carrera_id" integer NOT NULL REFERENCES "horario_carrera" ("id")
)
;
CREATE TABLE "horario_alumno" (
    "numero_control" varchar(8) NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL UNIQUE REFERENCES "auth_user" ("id"),
    "nombre" varchar(30) NOT NULL,
    "apellido_paterno" varchar(30) NOT NULL,
    "apellido_materno" varchar(30) NOT NULL,
    "genero" varchar(1) NOT NULL,
    "plan_estudio_id" varchar(15) NOT NULL REFERENCES "horario_planestudio" ("clave"),
    "email" varchar(75) NOT NULL,
    "telefono" varchar(15) NOT NULL,
    "is_active" bool NOT NULL
)
;
CREATE TABLE "horario_materia" (
    "id" integer NOT NULL PRIMARY KEY,
    "clave" varchar(7) NOT NULL,
    "nombre" varchar(65) NOT NULL,
    "creditos" integer NOT NULL,
    "ht" integer NOT NULL,
    "hp" integer NOT NULL,
    "hc" integer NOT NULL,
    "departamento_id" integer NOT NULL REFERENCES "horario_departamento" ("id"),
    "requisitos_id" integer REFERENCES "horario_materia" ("id"),
    "corequisitos_id" integer REFERENCES "horario_materia" ("id"),
    "plan_estudio_id" varchar(15) NOT NULL REFERENCES "horario_planestudio" ("clave"),
    UNIQUE ("clave", "plan_estudio_id")
)
;
CREATE TABLE "horario_aula" (
    "id" integer NOT NULL PRIMARY KEY,
    "edificio" varchar(1) NOT NULL,
    "numero" integer NOT NULL
)
;
CREATE TABLE "horario_profesor" (
    "rfc" varchar(13) NOT NULL PRIMARY KEY,
    "nombre" varchar(30) NOT NULL,
    "apellido_paterno" varchar(30) NOT NULL,
    "apellido_materno" varchar(30) NOT NULL,
    "genero" varchar(1) NOT NULL,
    "fecha_ingreso" date NOT NULL,
    "grado" varchar(50) NOT NULL,
    "domicilio" varchar(200) NOT NULL,
    "email" varchar(75) NOT NULL,
    "telefono" varchar(15) NOT NULL
)
;
CREATE TABLE "horario_clase" (
    "id" integer NOT NULL PRIMARY KEY,
    "materia_id" integer NOT NULL REFERENCES "horario_materia" ("id"),
    "profesor_id" varchar(13) NOT NULL REFERENCES "horario_profesor" ("rfc"),
    "hora" time NOT NULL,
    "periodo" varchar(30) NOT NULL,
    "anio" integer NOT NULL,
    "cupo" integer NOT NULL,
    UNIQUE ("materia_id", "profesor_id", "hora")
)
;
CREATE TABLE "horario_detallealumnomateria" (
    "id" integer NOT NULL PRIMARY KEY,
    "materia_id" integer NOT NULL REFERENCES "horario_materia" ("id"),
    "alumno_id" varchar(8) NOT NULL REFERENCES "horario_alumno" ("numero_control"),
    "situacion" integer NOT NULL
)
;
CREATE TABLE "horario_alumnoclase" (
    "id" integer NOT NULL PRIMARY KEY,
    "alumno_id" varchar(8) NOT NULL REFERENCES "horario_alumno" ("numero_control"),
    "clase_id" integer NOT NULL REFERENCES "horario_clase" ("id"),
    UNIQUE ("alumno_id", "clase_id")
)
;
CREATE INDEX "horario_carrera_cad1d7f2" ON "horario_carrera" ("departamento_id");
CREATE INDEX "horario_planestudio_1f799173" ON "horario_planestudio" ("carrera_id");
CREATE INDEX "horario_alumno_4f671892" ON "horario_alumno" ("plan_estudio_id");
CREATE INDEX "horario_materia_cad1d7f2" ON "horario_materia" ("departamento_id");
CREATE INDEX "horario_materia_3c3789d3" ON "horario_materia" ("requisitos_id");
CREATE INDEX "horario_materia_4d4234d0" ON "horario_materia" ("corequisitos_id");
CREATE INDEX "horario_materia_4f671892" ON "horario_materia" ("plan_estudio_id");
CREATE INDEX "horario_clase_ce3d2f7b" ON "horario_clase" ("materia_id");
CREATE INDEX "horario_clase_56d46bd1" ON "horario_clase" ("profesor_id");
CREATE INDEX "horario_detallealumnomateria_ce3d2f7b" ON "horario_detallealumnomateria" ("materia_id");
CREATE INDEX "horario_detallealumnomateria_fcd07de9" ON "horario_detallealumnomateria" ("alumno_id");
CREATE INDEX "horario_alumnoclase_fcd07de9" ON "horario_alumnoclase" ("alumno_id");
CREATE INDEX "horario_alumnoclase_6909f68a" ON "horario_alumnoclase" ("clase_id");
CREATE TABLE "auth_permission" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(50) NOT NULL,
    "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id"),
    "codename" varchar(100) NOT NULL,
    UNIQUE ("content_type_id", "codename")
)
;
CREATE TABLE "auth_group_permissions" (
    "id" integer NOT NULL PRIMARY KEY,
    "group_id" integer NOT NULL,
    "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"),
    UNIQUE ("group_id", "permission_id")
)
;
CREATE TABLE "auth_group" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(80) NOT NULL UNIQUE
)
;
CREATE TABLE "auth_user_groups" (
    "id" integer NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "group_id" integer NOT NULL REFERENCES "auth_group" ("id"),
    UNIQUE ("user_id", "group_id")
)
;
CREATE TABLE "auth_user_user_permissions" (
    "id" integer NOT NULL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id"),
    UNIQUE ("user_id", "permission_id")
)
;
CREATE TABLE "auth_user" (
    "id" integer NOT NULL PRIMARY KEY,
    "password" varchar(128) NOT NULL,
    "last_login" datetime NOT NULL,
    "is_superuser" bool NOT NULL,
    "username" varchar(30) NOT NULL UNIQUE,
    "first_name" varchar(30) NOT NULL,
    "last_name" varchar(30) NOT NULL,
    "email" varchar(75) NOT NULL,
    "is_staff" bool NOT NULL,
    "is_active" bool NOT NULL,
    "date_joined" datetime NOT NULL
)
;
CREATE INDEX "auth_permission_37ef4eb4" ON "auth_permission" ("content_type_id");
CREATE INDEX "auth_group_permissions_5f412f9a" ON "auth_group_permissions" ("group_id");
CREATE INDEX "auth_group_permissions_83d7f98b" ON "auth_group_permissions" ("permission_id");
CREATE INDEX "auth_user_groups_6340c63c" ON "auth_user_groups" ("user_id");
CREATE INDEX "auth_user_groups_5f412f9a" ON "auth_user_groups" ("group_id");
CREATE INDEX "auth_user_user_permissions_6340c63c" ON "auth_user_user_permissions" ("user_id");
CREATE INDEX "auth_user_user_permissions_83d7f98b" ON "auth_user_user_permissions" ("permission_id");
CREATE TABLE "home_maestro" (
    "id" integer NOT NULL PRIMARY KEY,
    "name" varchar(200) NOT NULL
)
;
CREATE TABLE "news_entry" (
    "id" integer NOT NULL PRIMARY KEY,
    "title" varchar(200) NOT NULL,
    "body" text NOT NULL,
    "published" bool NOT NULL,
    "created" datetime NOT NULL,
    "modified" datetime NOT NULL,
    "user_id" integer NOT NULL REFERENCES "auth_user" ("id")
)
;
CREATE INDEX "news_entry_6340c63c" ON "news_entry" ("user_id");

COMMIT;
