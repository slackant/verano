// Init file input plugin
$("#csv-input").fileinput({
  showCaption: false
});

$('#id_fecha_ingreso').datepicker({autoclose: true, language: 'es', daysOfWeekDisabled: [0, 6], endDate: '0d'});

// Go to section
function goToByScroll(id, time) {
    $('html,body').animate({ scrollTop: $("#"+id).offset().top -90}, time);
}
