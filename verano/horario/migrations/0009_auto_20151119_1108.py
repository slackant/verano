# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horario', '0008_auto_20151119_0435'),
    ]

    operations = [
        migrations.AddField(
            model_name='alumno',
            name='token',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='alumno',
            name='token_used',
            field=models.BooleanField(default=False),
        ),
    ]
