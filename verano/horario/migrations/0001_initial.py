# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('numero_control', models.CharField(primary_key=True, max_length=8, serialize=False, verbose_name='Numero de control')),
                ('nombre', models.CharField(max_length=30)),
                ('apellido_paterno', models.CharField(max_length=30)),
                ('apellido_materno', models.CharField(max_length=30)),
                ('genero', models.CharField(choices=[('F', 'Femenino'), ('M', 'Masculino')], max_length=1, verbose_name='genero')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Correo electronico')),
                ('telefono', models.CharField(max_length=15, verbose_name='telefono')),
                ('is_active', models.BooleanField(help_text='Elija esta opcion si el alumno puede inscribirse a algun curso de verano', default=True, verbose_name='Activo')),
            ],
        ),
        migrations.CreateModel(
            name='AlumnoClase',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('alumno', models.ForeignKey(to='horario.Alumno')),
            ],
            options={
                'verbose_name_plural': 'Enlistar alumno en una clase',
            },
        ),
        migrations.CreateModel(
            name='Aula',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('edificio', models.CharField(max_length=1)),
                ('numero', models.IntegerField(verbose_name='Numero de aula')),
            ],
        ),
        migrations.CreateModel(
            name='Carrera',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('nombre', models.CharField(unique=True, max_length=65, verbose_name='Nombre de la carrera')),
                ('coordinador', models.CharField(max_length=100, verbose_name='Nombre del coordinador')),
                ('presidente_academico', models.CharField(max_length=100, verbose_name='Nombre del presidente academico')),
            ],
        ),
        migrations.CreateModel(
            name='Clase',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('hora', models.TimeField()),
                ('periodo', models.CharField(max_length=30)),
                ('anio', models.IntegerField(verbose_name='Anho')),
                ('cupo', models.IntegerField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('nombre', models.CharField(unique=True, max_length=50, verbose_name='Nombre de departamento')),
                ('jefe', models.CharField(max_length=100, verbose_name='Nombre del jefe de departamento')),
            ],
        ),
        migrations.CreateModel(
            name='DetalleAlumnoMateria',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('situacion', models.IntegerField(choices=[(1, 'Aprobado'), (2, 'Curso Repite'), (3, 'Examen especial')], verbose_name='situacion')),
                ('alumno', models.ForeignKey(to='horario.Alumno')),
            ],
            options={
                'verbose_name_plural': 'Relacion Alumno-Materia',
            },
        ),
        migrations.CreateModel(
            name='Materia',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('clave', models.CharField(max_length=8)),
                ('nombre', models.CharField(max_length=65, verbose_name='materia')),
                ('semestre', models.PositiveIntegerField(choices=[(1, 'Uno'), (2, 'Dos'), (3, 'Tres'), (4, 'Cuantro'), (5, 'Quinto'), (6, 'Sexto'), (7, 'Septimo'), (8, 'Octavo'), (9, 'Nueve')])),
                ('creditos', models.PositiveIntegerField()),
                ('ht', models.PositiveIntegerField(verbose_name='horas-teoria')),
                ('hp', models.PositiveIntegerField(verbose_name='horas-practica')),
                ('hc', models.PositiveIntegerField()),
                ('corequisitos', models.ForeignKey(null=True, to='horario.Materia', blank=True, related_name='materia_corequisitos')),
                ('departamento', models.ForeignKey(to='horario.Departamento')),
            ],
        ),
        migrations.CreateModel(
            name='PlanEstudio',
            fields=[
                ('clave', models.CharField(primary_key=True, max_length=15, serialize=False)),
                ('carrera', models.ForeignKey(to='horario.Carrera', verbose_name='Carrera en la que se imparte')),
            ],
        ),
        migrations.CreateModel(
            name='Profesor',
            fields=[
                ('rfc', models.CharField(primary_key=True, max_length=13, serialize=False, verbose_name='RFC')),
                ('nombre', models.CharField(max_length=30)),
                ('apellido_paterno', models.CharField(max_length=30)),
                ('apellido_materno', models.CharField(max_length=30)),
                ('genero', models.CharField(choices=[('F', 'Femenino'), ('M', 'Masculino')], max_length=1)),
                ('fecha_ingreso', models.DateField(verbose_name='Fecha de ingreso')),
                ('grado', models.CharField(max_length=50)),
                ('domicilio', models.CharField(max_length=200)),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='Correo electronico')),
                ('telefono', models.CharField(max_length=15)),
                ('materias', models.ManyToManyField(related_name='materias', to='horario.Materia')),
            ],
            options={
                'verbose_name_plural': 'Profesores',
            },
        ),
        migrations.CreateModel(
            name='SolicitudCurso',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('hora', models.TimeField()),
                ('materia', models.OneToOneField(to='horario.Materia', related_name='solicitud_materia')),
                ('profesor', models.OneToOneField(to='horario.Profesor', related_name='solictud_profesor')),
            ],
        ),
        migrations.AddField(
            model_name='materia',
            name='plan_estudio',
            field=models.ForeignKey(to='horario.PlanEstudio'),
        ),
        migrations.AddField(
            model_name='materia',
            name='requisitos',
            field=models.ForeignKey(null=True, to='horario.Materia', blank=True, related_name='materia_requisitos'),
        ),
        migrations.AddField(
            model_name='detallealumnomateria',
            name='materia',
            field=models.ForeignKey(to='horario.Materia'),
        ),
        migrations.AddField(
            model_name='clase',
            name='materia',
            field=models.ForeignKey(to='horario.Materia'),
        ),
        migrations.AddField(
            model_name='clase',
            name='profesor',
            field=models.ForeignKey(to='horario.Profesor'),
        ),
        migrations.AddField(
            model_name='carrera',
            name='departamento',
            field=models.ForeignKey(to='horario.Departamento'),
        ),
        migrations.AddField(
            model_name='alumnoclase',
            name='clase',
            field=models.ForeignKey(to='horario.Clase'),
        ),
        migrations.AddField(
            model_name='alumno',
            name='plan_estudio',
            field=models.ForeignKey(to='horario.PlanEstudio'),
        ),
        migrations.AddField(
            model_name='alumno',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='materia',
            unique_together=set([('clave', 'plan_estudio')]),
        ),
        migrations.AlterUniqueTogether(
            name='clase',
            unique_together=set([('materia', 'profesor', 'hora')]),
        ),
        migrations.AlterUniqueTogether(
            name='alumnoclase',
            unique_together=set([('alumno', 'clase')]),
        ),
    ]
