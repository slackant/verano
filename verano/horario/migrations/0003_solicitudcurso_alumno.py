# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horario', '0002_solicitudcurso_aprobado'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitudcurso',
            name='alumno',
            field=models.OneToOneField(related_name='solicitud_alumno', to='horario.Alumno', default=None),
            preserve_default=False,
        ),
    ]
