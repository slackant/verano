# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horario', '0004_auto_20151012_0457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitudcurso',
            name='alumno',
            field=models.ForeignKey(to='horario.Alumno', related_name='solicitud_materia'),
        ),
    ]
