# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('horario', '0006_auto_20151117_0514'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='telefono',
            field=models.CharField(blank=True, max_length=15, verbose_name='telefono'),
        ),
        migrations.AlterField(
            model_name='alumno',
            name='user',
            field=models.OneToOneField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
