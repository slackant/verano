# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('horario', '0005_auto_20151012_0544'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alumno',
            name='apellido_materno',
        ),
        migrations.RemoveField(
            model_name='alumno',
            name='apellido_paterno',
        ),
        migrations.RemoveField(
            model_name='alumno',
            name='nombre',
        ),
        migrations.RemoveField(
            model_name='profesor',
            name='apellido_materno',
        ),
        migrations.RemoveField(
            model_name='profesor',
            name='apellido_paterno',
        ),
        migrations.RemoveField(
            model_name='profesor',
            name='nombre',
        ),
        migrations.AddField(
            model_name='alumno',
            name='nombre_completo',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profesor',
            name='nombre_completo',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
