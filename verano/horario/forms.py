# -*- coding: utf-8 -*-

import csv
from io import StringIO

from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import (Materia, Alumno, Carrera, Departamento, PlanEstudio,
                     Profesor, Profesor, Clase, SolicitudCurso)


class UploadFileForm(forms.Form):
    csv_file = forms.FileField(
        widget=forms.FileInput(attrs={'multiple': True, 'accept': '.csv,.xls',
                                      'class': 'file-loading', 'id': 'csv-input',
                                      'data-allowed-file-extensions': '["csv", "xls", "txt"]'}))


class ProfesorForm(forms.ModelForm):
    class Meta:
        model = Profesor
        fields = (
            'rfc', 'fecha_ingreso', 'nombre_completo', 'grado',
            'genero', 'domicilio', 'telefono', 'materias'
        )

    def __init__(self, *args, **kwargs):
        super(ProfesorForm, self).__init__(*args, **kwargs)
        self.fields['rfc'].widget.attrs['class'] = 'form-control'
        self.fields['nombre_completo'].widget.attrs['class'] = 'form-control'
        self.fields['genero'].widget.attrs['class'] = 'form-control'
        self.fields['domicilio'].widget.attrs['class'] = 'form-control'
        self.fields['telefono'].widget.attrs['class'] = 'form-control'
        self.fields['materias'].widget.attrs['class'] = 'form-control'
        self.fields['grado'].widget.attrs['class'] = 'form-control'
        self.fields['fecha_ingreso'].widget.attrs['class'] = 'form-control'
        self.fields['fecha_ingreso'].widget.attrs['data-provide'] = 'datepicker'


class PlanEstudioForm(forms.ModelForm):
    class Meta:
        model = PlanEstudio
        fields = ('carrera', 'clave',)

    def __init__(self, *args, **kwargs):
        super(PlanEstudioForm, self).__init__(*args, **kwargs)
        self.fields['carrera'].widget.attrs['class'] = 'form-control'
        self.fields['clave'].widget.attrs['class'] = 'form-control'

    #@property
    #def helper(self):
    #    helper = FormHelper()
    #    helper.jade5_required = True
    #    helper.attrs = {"name": 'create_plan'}

    #    helper.layout = Layout(
    #        HTML("""<h3>Registrar Plan de estudios</h3>"""),

    #        Div(Field('clave', id="clave"),
    #            css_class="col-6 col-lg-6 col-md-6 col-sm-6"),

    #        Div('carrera', 
    #            HTML("""<a id='comment-button' class="btn" href="{% url 'add-carrera' %}?next={% url 'add-plan' %}">
    #                <span class="glyphicon glyphicon-plus-sign"></span>
    #                </a>"""),
    #            css_class='col-lg-6'
    #        ),

    #        HTML("""<br>"""),
    #        Submit('save_account', 'Guardar', css_class="btn white")
    #    )
    #    return helper
        

class MateriaForm(forms.ModelForm):
    class Meta:
        model = Materia
        fields = (
            'plan_estudio', 'clave', 'nombre', 'departamento', 'creditos',
            'corequisitos', 'requisitos', 'ht', 'hp', 'hc', 'semestre'
        )

    def __init__(self, *args, **kwargs):
        super(MateriaForm, self).__init__(*args, **kwargs)
        self.fields['plan_estudio'].widget.attrs['class'] = 'form-control'
        self.fields['corequisitos'].widget.attrs['class'] = 'form-control'
        self.fields['requisitos'].widget.attrs['class'] = 'form-control'
        self.fields['ht'].widget.attrs['class'] = 'form-control'
        self.fields['hp'].widget.attrs['class'] = 'form-control'
        self.fields['hc'].widget.attrs['class'] = 'form-control'
        self.fields['clave'].widget.attrs['class'] = 'form-control'
        self.fields['nombre'].widget.attrs['class'] = 'form-control'
        self.fields['departamento'].widget.attrs['class'] = 'form-control'
        self.fields['creditos'].widget.attrs['class'] = 'form-control'
        self.fields['semestre'].widget.attrs['class'] = 'form-control'

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.attrs = {"name": 'create_materia'}

    #     helper.layout = Layout(
    #         HTML("""<h3>Registrar Materia</h3>"""),

    #         HTML("""<div class="row"><legend>Informacion principal</legend>"""),
    #         Div('plan_estudio', 
    #             css_class='col-lg-6'
    #         ),
    #         Div('semestre', 
    #             css_class='col-lg-6'
    #         ),
    #         Div(Field('clave', id="clave"),
    #             css_class="col-6 col-lg-6 col-md-6 col-sm-6"),

    #         Div('nombre', css_class="col-6 col-lg-6 col-md-6 col-sm-6"),
    #         HTML("""</div>"""),

    #         HTML("""<div class="row"><legend>Datos de localizacion</legend>"""),
    #         Div('departamento',
    #             HTML("""<a class="btn" href="{% url 'add-depto' %}?next={% url 'add-materia' %}">
    #                 <span class="glyphicon glyphicon-plus-sign"></span></a>"""),
    #             css_class='col-lg-4'),
    #         Div('requisitos', css_class='col-lg-4'),
    #         Div('corequisitos', css_class='col-lg-4'),
    #         HTML("""</div>"""),
    #         HTML("""<div class="row"><legend>Mas info</legend>"""),
    #         Div('creditos', css_class="col-3 col-lg-3 col-md-3 col-sm-3"),
    #         Div('ht', css_class="col-3 col-lg-3 col-md-3 col-sm-3"),
    #         Div('hp', css_class="col-3 col-lg-3 col-md-3 col-sm-3"),
    #         Div('hc', css_class="col-3 col-lg-3 col-md-3 col-sm-3"),

    #         HTML("""</div>"""),

    #         Submit('save_account', 'Guardar', css_class="btn white")
    #     )
    #     return helper

class ClaseForm(forms.ModelForm):
    class Meta:
        model = Clase
        fields = ('anio', 'periodo', 'hora', 'materia', 'profesor', 'cupo',)

    def __init__(self, *args, **kwargs):
        super(ClaseForm, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs['class'] = 'form-control'
        self.fields['periodo'].widget.attrs['class'] = 'form-control'
        self.fields['hora'].widget.attrs['class'] = 'form-control'
        self.fields['materia'].widget.attrs['class'] = 'form-control'
        self.fields['profesor'].widget.attrs['class'] = 'form-control'
        self.fields['cupo'].widget.attrs['class'] = 'form-control'

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.layout = Layout(
    #         HTML("""<h3>Agregar Clase</h3>"""),
    #         Div('anio', css_class='col-4 col-lg-4 col-md-3 col-sm-3'),
    #         Div('periodo', css_class='col-4 col-lg-4 col-md-3 col-sm-3'),
    #         Div('hora', css_class='col-4 col-lg-4 col-md-3 col-sm-3'),
    #         Div('materia',
    #             HTML("""<a id='button-materia' class="btn" href="{% url 'add-materia' %}?next={% url 'add-clase' %}">
    #                 <span class="glyphicon glyphicon-plus-sign"></span>
    #                 </a>"""),
    #             css_class='col-6 col-lg-6 col-md-3 col-sm-3'),
    #         Div('profesor',
    #             HTML("""<a id='button-profesor' class="btn" href="{% url 'add-profesor' %}?next={% url 'add-clase%}">
    #                 <span class="glyphicon glyphicon-plus-sign"></span>
    #                 </a>"""),
    #             css_class='col-6 col-lg-6 col-md-3 col-sm-3'),
    #         'cupo',
    #         Submit('save_materia', 'Guardar', css_class='btn white')
    #     )
    #     return helper


class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = ('nombre', 'jefe',)

    def __init__(self, *args, **kwargs):
        super(DepartamentoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['class'] = 'form-control'
        self.fields['jefe'].widget.attrs['class'] = 'form-control'

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.attrs = {'name': 'create_depto'}

    #     helper.layout = Layout(
    #         HTML("""<h3 class="">Registrar Depto</h3>"""),

    #         Div('nombre', css_class='col-lg-6'),
    #         Div('jefe', css_class='col-lg-6'),
    #         Submit('save_depto', 'Guardar depto.', css_class="btn white")
    #     )
    #     return helper


class CarreraForm(forms.ModelForm):
    class Meta:
        model = Carrera
        fields = (
            'nombre', 'departamento', 'coordinador', 'presidente_academico',
        )

    def __init__(self, *args, **kwargs):
        super(CarreraForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['class'] = 'form-control'
        self.fields['departamento'].widget.attrs['class'] = 'form-control'
        self.fields['coordinador'].widget.attrs['class'] = 'form-control'
        self.fields['presidente_academico'].widget.attrs['class'] = 'form-control'

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.layout = Layout(
    #         HTML("""<h3 class="">Registrar Carrera</h3>"""),

    #         Div('nombre', css_class='col-lg-6'),
    #         Div('departamento', 
    #             HTML("""<a id='button-depto' class="btn" href="{% url 'add-depto' %}?next={% url 'add-carrera' %}">
    #                 <span class="glyphicon glyphicon-plus-sign"></span>
    #                 </a>"""),
    #             css_class='col-lg-6'
    #         ),
    #         Div('coordinador', css_class="col-lg-6"),
    #         Div('presidente_academico', css_class="col-lg-6"),

    #         Submit('save_account', 'Guardar carrera', css_class="btn white")
    #     )
    #     return helper


class AlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = ('nombre_completo', 'numero_control', 'plan_estudio', 'is_active',
                  'telefono', 'genero', 'email', 'user',)

    def __init__(self, *args, **kwargs):
        super(AlumnoForm, self).__init__(*args, **kwargs)
        self.fields['nombre_completo'].widget.attrs['class'] = 'form-control'
        self.fields['numero_control'].widget.attrs['class'] = 'form-control'
        self.fields['plan_estudio'].widget.attrs['class'] = 'form-control'
        self.fields['is_active'].widget.attrs['class'] = 'form-control'
        self.fields['telefono'].widget.attrs['class'] = 'form-control'
        self.fields['genero'].widget.attrs['class'] = 'form-control'
        self.fields['email'].widget.attrs['class'] = 'form-control'
        self.fields['user'].widget.attrs['class'] = 'form-control'

    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.jade5_required = True
    #     helper.attrs = {"name": 'create_alumno'}

    #     helper.layout = Layout(
    #         HTML("""<h3>Registrar Alumno</h3>"""),

    #         HTML("""<legend>Informacion academica</legend>"""),
    #         Div(Field('user'),
    #             HTML("""<a id='button-user' class="btn" href="{% url 'create-user' %}?next={% url 'add-alumno' %}">
    #             <span class="glyphicon glyphicon-plus-sign"></span>
    #             </a>"""),
    #             css_class='col-4 col-lg-4 col-md-3 col-sm-3'
    #         ),
    #         Div(Field('numero_control', id="no_control"),
    #             css_class='col-4 col-lg-4 col-md-3 col-sm-3'),
    #         Div('plan_estudio', 
    #             HTML("""<a id='button-plan' class="btn" href="{% url 'add-plan' %}?next={% url 'add-alumno' %}">
    #                 <span class="glyphicon glyphicon-plus-sign"></span>
    #                 </a>"""),
    #             css_class='col-4 col-lg-4 col-md-3 col-sm-3'
    #         ),

    #         HTML("""<div><legend>Informacion personal</legend>"""),
    #         Div('nombre', css_class="col-4 col-lg-4 col-md-4 col-sm-4"),
    #         Div('genero', css_class="col-4 col-lg-4 col-md-4 col-sm-4"),
    #         Div('email', css_class="col-4 col-lg-4 col-md-4 col-sm-4"),
    #         Div('telefono', css_class="col-4 col-lg-4 col-md-4 col-sm-4"),
    #         HTML("""</div>"""),

    #         Div('is_active', css_class='col-6 col-lg-6 col-md-6 col-sm-6'),
    #         Submit('save_account', 'Crear cuenta', css_class="btn white"),
    #     )
    #     return helper


class RequestMateriaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(RequestMateriaForm, self).__init__(*args, **kwargs)
        self.fields['hora'].widget.attrs['class'] = 'form-control'
        self.fields['profesor'].widget.attrs['class'] = 'form-control'
        self.fields['alumno'].widget.attrs['hidden'] = True

    class Meta:
        model = SolicitudCurso
        exclude = ()
        widgets = {
            'materia': forms.widgets.TextInput(attrs={'class': 'form-control',
                                                      'readonly': True}),
        }
