# -*- coding: utf-8 -*-

import csv
from io import StringIO
from uuid import uuid4

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ValidationError
from django.db.models import Q
from django.views.generic import (ListView, CreateView, FormView, UpdateView,
        DeleteView)

from .forms import *

from .models import Alumno, Carrera, Departamento

from accounts.views import NextUrlMixin
from home.views import StaffRequiredMixin


class PlanEstudioListView(ListView):
    template_name = 'horario/plan_estudio.jade'
    success_url = reverse_lazy('index')
    model = Materia

    def get_queryset(self):
        queryset = super(PlanEstudioListView, self).get_queryset()

        no_control = self.request.user.username
        try:
            alumno = Alumno.objects.get(numero_control__exact=no_control)
            return queryset.filter(plan_estudio__exact=alumno.plan_estudio)
        except:
            return queryset.none()


class UploadFileView(FormView):
    template_name = 'horario/up.jade'
    form_class = UploadFileForm
    success_url = reverse_lazy('index')

    def post(self, request, *args, **kwargs):
        planes = PlanEstudio.objects.all()
        files = request.FILES.getlist('csv_file')
        errors = set()
        new_alumnos = []
        for file_input in files:
            csv_file = StringIO(file_input.read().decode('latin-1'))
            reader = csv.reader(csv_file)

            for item in reader:
                try:
                    plan = planes.get(clave=item[3])
                    if not Alumno.objects.filter(pk=item[1], plan_estudio=plan).exists():
                        alumno = Alumno(nombre_completo=item[1], genero=item[2],
                                        plan_estudio=plan, is_active=False,
                                        numero_control=item[0], token=uuid4())
                        new_alumnos.append(alumno)
                except PlanEstudio.DoesNotExist:
                    errors.add(item[3])

        Alumno.objects.bulk_create(new_alumnos)
        msg = messages.success(request, '%d Alumnos creados' % len(new_alumnos))

        if len(errors) > 0:
            messages.error(request, 'Los sig. planes de estudios no existen: %s' % errors)
            return self.form_invalid(self.get_form())

        return super(UploadFileView, self).post(request, *args, **kwargs)


class CreateDepartamentoView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = 'horario/create_depto.jade'
    form_class = DepartamentoForm
    # success_url = reverse_lazy('add-carrera')
    model = Departamento
    success_message = 'Departamento agregado'


class DeleteDepartamentoView(StaffRequiredMixin, DeleteView):
    model = Departamento
    template_name = "horario/delete.jade"
    success_url = reverse_lazy('index')
    breadcrumbs = "Eliminar departamento"


class DeleteMateriaDelView(StaffRequiredMixin, DeleteView):
    model = Materia
    template_name = "horario/delete.jade"
    success_url = reverse_lazy('index')
    breadcrumbs = "Eliminar Materia"


class CreateCarreraView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = 'horario/create_carrera.jade'
    form_class = CarreraForm
    # success_url = reverse_lazy('add-alumno')
    model = Carrera
    success_message = 'Carrera agregada'


class CreateAlumnoView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = "horario/create_alumno.jade"
    form_class = AlumnoForm
    # success_url = reverse_lazy('index')
    model = Alumno
    success_message = 'Alumno agregado'


class DeleteAlumnoView(NextUrlMixin, DeleteView):
    from django.contrib.auth.models import User
    model = User
    template_name = "horario/delete-u.jade"
    success_url = reverse_lazy('index')
    breadcrumbs = "Eliminar alumno"


class ListaAlumnoListView(ListView):
    template_name = "horario/lista_alumno.jade"
    model = Alumno


class MateriaListView(ListView):
    template_name = "horario/lista_materias.jade"
    model = Materia


class EditAlumnoView(NextUrlMixin, UpdateView):
    template_name = "horario/edit_alumno.jade"
    form_class = AlumnoForm
    success_url = reverse_lazy('index')
    model = Alumno


class EditClaseView(NextUrlMixin, UpdateView):
    template_name = "horario/edit_clase.jade"
    form_class = ClaseForm
    success_url = reverse_lazy('index')
    model = Clase


class EditMateriaView(NextUrlMixin, UpdateView):
    template_name = "horario/edit_materia.jade"
    form_class = MateriaForm
    success_url = reverse_lazy('index')
    model = Materia


class CreateMateriaView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = "horario/create_materia.jade"
    form_class = MateriaForm
    # success_url = reverse_lazy('add-alumno')
    model = Materia
    success_message = 'Materia agregada'


class CreatePlanEstudioView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = 'horario/create_plan.jade'
    form_class = PlanEstudioForm
    # success_url = reverse_lazy('add-alumno')
    model = PlanEstudio
    success_message = 'Plan de estudio agregado'


class CreateProfesorView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = 'horario/create_profesor.jade'
    form_class = ProfesorForm
    # success_url = reverse_lazy('add-alumno')
    model = Profesor
    success_message = 'Profesor agregado'

class CreateClaseView(NextUrlMixin, SuccessMessageMixin, CreateView):
    template_name = 'horario/create_clase.jade'
    form_class = ClaseForm
    # success_url = reverse_lazy('add-alumno')
    model = Clase
    success_message = 'Clase agregada'


class SearchAlumnoView(ListView):
    """Allows to filter an Alumno object"""
    model = Alumno
    template_name = 'horario/alumno_details.jade'

    def get_queryset(self):
        queryset = super(SearchAlumnoView, self).get_queryset()
        q = self.request.GET.get('q')
        if q:
            try:
                alumno = queryset.get(Q(numero_control__exact=q))
            except Alumno.DoesNotExist:
                return None

            return alumno

        return queryset


class RequestMateriaView(SuccessMessageMixin, CreateView):
    template_name = 'horario/request_materia.jade'
    form_class = RequestMateriaForm
    model = SolicitudCurso
    success_url = reverse_lazy('index')
    success_message = 'Solicitud de materia agregada'

    def get_form_kwargs(self):
        # ctx = super(RequestMateriaView, self).get_form_kwargs()
        ctx = {'initial': {}, 'instance': None, 'prefix': None}
        if self.request.method == 'GET':
            try:
                materia = Materia.objects.get(pk=self.kwargs['materia_id'])
                ctx['initial']['materia'] = materia.pk
            except Materia.DoesNotExist:
                pass

            try:
                ctx['initial']['alumno'] = self.request.user.alumno.pk
            except:
                pass
        elif self.request.method == 'POST':
            ctx['data'] = self.request.POST
        print('CTX', ctx)

        return ctx

    def get_form(self, *args, **kwargs):
        form = super(RequestMateriaView, self).get_form(*args, **kwargs)

        materia_id = self.kwargs.get('materia_id')
        profesor_qs = Profesor.objects.filter(Q(materias__pk=materia_id))
        form.fields['profesor'].queryset = profesor_qs

        return form

    def get(self, request, *args, **kwargs):
        return super(RequestMateriaView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        return super(RequestMateriaView, self).form_valid(form)
