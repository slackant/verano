# -*- coding: utf-8 -*-

from django.db import models

# from accounts.models import UserRegister as User
from django.contrib.auth.models import User

from django.core.exceptions import ValidationError
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType


class Departamento(models.Model):
    nombre = models.CharField(verbose_name='Nombre de departamento',max_length=50, unique=True)
    jefe = models.CharField(max_length=100, verbose_name='Nombre del jefe de departamento')

    def __str__(self):
        return self.nombre


class Carrera(models.Model):
    nombre = models.CharField('Nombre de la carrera', max_length=65, unique=True)
    departamento = models.ForeignKey(Departamento)
    coordinador = models.CharField("Nombre del coordinador", max_length=100)
    presidente_academico = models.CharField('Nombre del presidente academico', max_length=100)
    # mision = models.TextField()
    # vision = models.TextField()

    def __str__(self):
        return self.nombre

    class Meta:
        pass


class PlanEstudio(models.Model):
    clave = models.CharField(max_length=15, primary_key=True)
    carrera = models.ForeignKey(Carrera, verbose_name="Carrera en la que se imparte")

    def __str__(self):
        return self.clave


GENEROS = (
    ('F', 'Femenino'),
    ('M', 'Masculino'),
)

ESTADOS = (
    ('A', 'Activo'),
    ('S', 'Suspendido'),
)

class Alumno(models.Model):
    numero_control = models.CharField(max_length=8,
            primary_key=True,
            verbose_name="Numero de control")

    user = models.OneToOneField(User, null=True, blank=True)

    nombre_completo = models.CharField(max_length=200)
#    curp = models.CharField(max_length=18, verbose_name="CURP")
    genero = models.CharField('genero', max_length=1, choices=GENEROS)
#    fecha_ingreso = models.DateField(verbose_name="Fecha de ingreso")

    plan_estudio = models.ForeignKey(PlanEstudio)
    # estado = models.CharField(max_length=1, choices=ESTADOS)

    # folio = models.CharField(max_length=200)
    email = models.EmailField("Correo electronico", blank=True)
    telefono = models.CharField('telefono', max_length=15, blank=True)

    is_active = models.BooleanField(
        'Activo', default=True,
        help_text='El alumno puede inscribirse a algún curso de verano')
    token = models.CharField(max_length=200, null=True)
    token_used = models.BooleanField(default=False)
    # is_staff = models.BooleanField('Super usuario?')

    def __str__(self):
        return self.numero_control + ' - ' + self.nombre_completo


class Materia(models.Model):
    SEMESTRES = (
        (1, 'Uno'),
        (2, 'Dos'),
        (3, 'Tres'),
        (4, 'Cuantro'),
        (5, 'Quinto'),
        (6, 'Sexto'),
        (7, 'Septimo'),
        (8, 'Octavo'),
        (9, 'Nueve'),
    )
    clave = models.CharField(max_length=8)
    nombre = models.CharField('materia', max_length=65)
    semestre = models.PositiveIntegerField(choices=SEMESTRES)
    creditos = models.PositiveIntegerField()
    ht = models.PositiveIntegerField('horas-teoria')
    hp = models.PositiveIntegerField('horas-practica')
    hc = models.PositiveIntegerField()

    departamento = models.ForeignKey(Departamento)
    # OR = models.IntegerField()
    # np = models.IntegerField()
    # r = models.IntegerField('%R')

    requisitos = models.ForeignKey('Materia', related_name='materia_requisitos', blank=True, null=True)
    corequisitos = models.ForeignKey('Materia', related_name='materia_corequisitos', blank=True, null=True)

    plan_estudio = models.ForeignKey(PlanEstudio)

    # numero_unidades = models.IntegerField('Numero de unidades')

    def __str__(self):
        return self.clave + ' ' + self.nombre

    class Meta:
        unique_together = ('clave', 'plan_estudio')


class Aula(models.Model):
    edificio = models.CharField(max_length=1)
    numero = models.IntegerField(verbose_name='Numero de aula')

    def __str__(self):
        return self.edificio + "-" + str(self.numero)


class Profesor(models.Model):
    rfc = models.CharField(max_length=13, primary_key=True, verbose_name="RFC")
    nombre_completo = models.CharField(max_length=200)

    genero = models.CharField(max_length=1, choices=GENEROS)
    fecha_ingreso = models.DateField(verbose_name="Fecha de ingreso")

    grado = models.CharField(max_length=50)
    materias = models.ManyToManyField(Materia,related_name="materias")

    domicilio = models.CharField(max_length=200)
    email = models.EmailField(blank=True, verbose_name='Correo electronico')
    telefono = models.CharField(max_length=15)

    class Meta:
        verbose_name_plural = "Profesores"

    def __str__(self):
        return self.rfc + ' - ' + self.nombre_completo


class Clase(models.Model):
    materia = models.ForeignKey(Materia)
    profesor = models.ForeignKey(Profesor)
    # aula = models.ForeignKey(Aula)
    hora = models.TimeField()
    periodo = models.CharField(max_length=30)
    anio = models.IntegerField(verbose_name="Anho")
    # materia_requerida = models.ForeignKey(Materia, related_name='materia_requerida', null=True, blank=True)
    cupo = models.IntegerField(blank=True)

    class Meta:
         unique_together = (('materia', 'profesor', 'hora',),)

    def __str__(self):
        return (self.materia.nombre + " -- " + str(self.hora) + " < " +
                self.profesor.nombre)

SITUACIONES = (
    (1,"Aprobado"),
    (2,"Curso Repite"),
    (3,"Examen especial"),
)

class DetalleAlumnoMateria(models.Model):
    materia = models.ForeignKey(Materia)
    alumno = models.ForeignKey(Alumno)
    situacion = models.IntegerField(choices=SITUACIONES, verbose_name='situacion')

    def __str__(self):
        return self.materia.nombre + " -- " + self.alumno.nombre_completo()

    class Meta:
        verbose_name_plural = "Relacion Alumno-Materia"


class AlumnoClase(models.Model):
    alumno = models.ForeignKey(Alumno)
    clase = models.ForeignKey(Clase)

    class Meta:
        verbose_name_plural = "Enlistar alumno en una clase"
        unique_together = (('alumno', 'clase',),)

    def __str__(self):
        return (self.alumno.numero_control + " -- " + self.clase.materia.nombre + " < " +
                self.alumno.nombre_completo)

    def save(self):
        detalles_alumno = DetalleAlumnoMateria.objects.filter(alumno=self.alumno)
        materia_req = self.clase.materia_requerida

        try:
            if len(detalles_alumno) > 0:
#                for i in detalles_alumno:
#                    if i.materia == self.clase.materia
#                        return 

                for i in detalles_alumno:
                    if (i.materia == materia_req and self.alumno.estado == 'A'
                            and i.situacion < 3 and self.clase.cupo > 0):
                        super(AlumnoClase, self).save()
                        self.clase.cupo = self.clase.cupo - 1
            else:
                if self.alumno.estado == 'A':
                    super(AlumnoClase, self).save()

        except:
            pass


class SolicitudCurso(models.Model):
    alumno = models.ForeignKey(Alumno, related_name="solicitud_materia")
    materia = models.OneToOneField(Materia, related_name="solicitud_materia")
    profesor = models.OneToOneField(Profesor,related_name="solictud_profesor")
    hora = models.TimeField()
    aprobado = models.BooleanField(default=False)

    def __str__(self):
        return '{} - {} - {} - {}'.format(self.alumno.nombre,
                                          self.materia.nombre,
                                          self.profesor.nombre,
                                          self.hora)

    class Meta:
        unique_together=('alumno' , 'materia')
