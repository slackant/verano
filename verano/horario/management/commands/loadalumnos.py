# -*- coding: utf-8 -*-

import csv

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from horario.models import Alumno


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('data/alumnos.csv') as alumnos_file:
            alumnos_reader = csv.reader(alumnos_file, delimiter=',')
            lista_alumnos = []

            for alumno in alumnos_reader:
                new_alumno = Alumno(nombre_completo=alumno[1],
                                    numero_control=alumno[0],
                                    genero=alumno[2])
                lista_alumnos.append(new_alumno)

            Alumno.objects.bulk_create(lista_alumnos)
