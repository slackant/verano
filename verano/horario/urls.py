# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import *

urlpatterns = [
    url(r'^alumno/plan/$', login_required(PlanEstudioListView.as_view()),
        name="plan"),
    url(r'^alumno/plan/(?P<pk>\d+)/$', login_required(PlanEstudioListView.as_view()),
        name="plan-b"),
    url(r'^alumno/lista/$',ListaAlumnoListView.as_view (), name="lista-alumno"),
    url(r'^materia/lista/$', MateriaListView.as_view (), name="lista-materia"),

    url(r'^alumno/import/$', login_required(UploadFileView.as_view()),
        name="import-data"),
    url(r'^alumno/add/$', login_required(CreateAlumnoView.as_view()),
        name="add-alumno"),
    url(r'^alumno/edit/(?P<pk>\d+)/$',
        login_required(EditAlumnoView.as_view()), name="edit-alumno"),
    url(r'^alumno/delete/(?P<pk>\d+)/$',
        login_required(DeleteAlumnoView.as_view()), name="delete-alumno"),

    url(r'^carrera/add/$', login_required(CreateCarreraView.as_view()),
        name="add-carrera"),
    url(r'^departamento/add/$',
        login_required(CreateDepartamentoView.as_view()), name="add-depto"),
    url(r'^departamento/delete/(?P<pk>\d+)/$',
        login_required(DeleteDepartamentoView.as_view()), name="delete-depto"),

    url(r'^profesor/add/$', login_required(CreateProfesorView.as_view()),
        name="add-profesor"),

    url(r'^materia/add/$', login_required(CreateMateriaView.as_view()),
        name="add-materia"),
    url(r'^request/materia/(?P<materia_id>\d+)/$',
        login_required(RequestMateriaView.as_view()), name="request-materia"),
    url(r'^request/materia/delete/(?P<materia_id>\d+)/$',
        login_required(DeleteMateriaDelView.as_view()), name="delete-materia"),
    url(r'^materia/edit/(?P<pk>\d+)/$',
        login_required(EditMateriaView.as_view()), name="edit-materia"),

    url(r'^clase/add/$', login_required(CreateClaseView.as_view()),
        name="add-clase"),
    url(r'^clase/edit/(?P<pk>\d+)/$', login_required(EditClaseView.as_view()),
        name="edit-clase"),

    url(r'^plan_estudio/add/$',
        login_required(CreatePlanEstudioView.as_view()), name="add-plan"),
    url(r'alumno/search/$', login_required(SearchAlumnoView.as_view()),
        name="search-alumno"),
]
